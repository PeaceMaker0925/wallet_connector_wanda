import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final qrcodeProvider = StateNotifierProvider.autoDispose<qrcodeNotifier, String>((ref) {
  return qrcodeNotifier();
});

class qrcodeNotifier extends StateNotifier<String> {
  qrcodeNotifier(): super('');

  void getQrcodeResult({required String result}) {
    try{
      print('#$result');
      state = result;
    }
    catch(e){
      print(e);
    }
  }


}