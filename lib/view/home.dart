import 'dart:convert';
import 'package:bip39/bip39.dart' as bip39;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:wallet_connector_wanda/view/wallet_connect_main.dart';
import 'package:web3dart/credentials.dart';
import 'package:convert/convert.dart';
import 'package:dart_bip32_bip44/dart_bip32_bip44.dart';
import 'package:ed25519_hd_key/ed25519_hd_key.dart';

class Home extends ConsumerWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Wallet Info'),
        actions: <Widget>[
          IconButton(
            constraints: const BoxConstraints.expand(width: 80),
            icon: const Text('WC', textAlign: TextAlign.center),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const WalletConnectMain()),
              );
            },
          ),
        ],
      ),
      body: FutureBuilder<dynamic>(
        future: createWallet(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (!snapshot.hasData) {
            // while data is loading:
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            // data loaded:
            final address = snapshot.data;
            return Center(
              child: Text('$address'),
            );
          }
        },
      ),
    );
  }
}

Future<dynamic> createWallet() async {
  //產生助記詞
  String randomMnemonic = bip39.generateMnemonic();
  print(randomMnemonic);
  //字串轉成128Byte的16進制數字
  String seed = bip39.mnemonicToSeedHex(randomMnemonic);
  print(seed);
  // 產生種子
  Chain chain = Chain.seed(seed);
  // 產生MasterKey and Path
  // the wallet for data on Ethereum.
  var key = chain.forPath("m/44'/60'/0'/0/0") as ExtendedPrivateKey;
  print(key);

  var cryptMnemonic = bip39.mnemonicToEntropy(randomMnemonic);
  var privateKey = await getPrivateKey(randomMnemonic);
  var private = EthPrivateKey.fromHex(privateKey);
  var address = await private.extractAddress();
  print('address: $address');
  return address;
}

Future<String> getPrivateKey(String mnemonic) async {
  final seed = bip39.mnemonicToSeedHex(mnemonic);
  final master = await ED25519_HD_KEY.getMasterKeyFromSeed(hex.decode(seed),
      masterSecret: 'Bitcoin seed');
  final privateKey = hex.encode(master.key);
  print('private: $privateKey');
  return privateKey;
}