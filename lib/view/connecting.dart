import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';

import '../qrcode_provider.dart';

enum ConnectionState {
  disconnected,
  connecting,
  connected,
  connectionFailed,
  connectionCancelled,
}

const String walletAddress = '0x42FA3Bb8A03178499123ffA44f62b129eB91dCb6';
const int chainId = 1;

class Connecting extends ConsumerStatefulWidget {
  const Connecting({super.key});
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _ConnectingState();
}

class _ConnectingState extends ConsumerState<Connecting> {

  late final WalletConnect connector;
  ConnectionState _state = ConnectionState.disconnected;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_){
      String result = ref.watch(qrcodeProvider);
      print('@$result');
      connector = WalletConnect(
        uri: result,
        clientMeta: const PeerMeta(
          name: 'wanda_flutter_walletconnect',
          description: 'Wanda\'s WalletConnect Developer App',
          url: 'https://walletconnect.org',
          icons: [
            'https://i.imgur.com/J6HrlRl.jpg'
          ],
        ),
      );
      connector.on('session_request', (payload) async {
        print('@$payload');
        switch (payload.runtimeType) {
          case WCSessionRequest:
          // do something
            setState((){
              _state = ConnectionState.connecting;
            });

            await connector.approveSession(chainId: chainId, accounts: [walletAddress]);
            break;
          default: { print("Invalid choice"); }
          break;
        }
      });

      connector.on('disconnect', (session) async{
        setState((){
          _state = ConnectionState.disconnected;
        });
      } );

      connector.on('session_update', (session) async{
        setState((){
          print("session_update: $session");
        });
      } );
      connector.registerListeners(
          onSessionUpdate: (payload) async{ print('session_update: $payload'); },
          onConnect: (connect) async{
            print('@$connect');
            setState((){
              _state = ConnectionState.connected;
            });
          },
          onDisconnect: () async{ print("disconnected."); }
      );
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Connecting'),
        ),
        body: Center(
          child: Column(
            // child: Text("")
              children: [
                Text(_transactionStateToString(state: _state)),
                if(_state == ConnectionState.connected)TextButton(
                  child: const Text('Disconnect.'),
                  onPressed: () async {
                    await connector.killSession(SessionStatus(chainId: chainId, accounts: [walletAddress]));
                  },

                ),
                TextButton(
                  child: const Text('Update'),
                  onPressed: () async {
                    await connector.updateSession(SessionStatus(chainId: chainId, accounts: [walletAddress]));
                  },

                )
              ]
          ),
        )
    );
  }

  @override
  dispose() {
    // await connector.killSession();
    super.dispose();
  }
  String _transactionStateToString({required ConnectionState state}) {
    print('new state: $state');
    switch (state) {
      case ConnectionState.disconnected:
        return 'Disconnected';
      case ConnectionState.connecting:
        return 'Connecting';
      case ConnectionState.connected:
        return 'Session connected';
      case ConnectionState.connectionFailed:
        return 'Connection failed';
      case ConnectionState.connectionCancelled:
        return 'Connection cancelled';
    }
  }

}